package org.webswing.server.services.websocket;

import org.webswing.server.base.UrlHandler;

public interface WebSocketUrlHandler extends UrlHandler, WebSocketMessageListener {

}
